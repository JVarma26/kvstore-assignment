// Design an efficient program that prints out, in reverse order,
// every multiple of 7 that is between 1 and 300. Extend the program to other multiples
// and number ranges. Write the program in any programming language of your choice.

#include<stdio.h>

// To print the multiples of given number with given range
void findMultiplesByReverseOrder(int range, int multipleOf)
{
	if(range == 0 || multipleOf == 0){
		return;	
	}

	int i, a = (range % multipleOf == 0) ? range :(range-(range % multipleOf));

	for (i = a; i >= multipleOf; i-=multipleOf) 
	{
		printf("reverseNo multipal of = \t %d \n", i);
	}

}

int main(){
	findMultiplesByReverseOrder(300, 7);
}	

