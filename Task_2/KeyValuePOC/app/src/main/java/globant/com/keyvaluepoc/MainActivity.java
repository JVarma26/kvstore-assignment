package globant.com.keyvaluepoc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        KeyValueManager keyValueManager = KeyValueManager.getInstance(this);

        keyValueManager.putBoolean("Hello", true);
        keyValueManager.putString("stringKey", "test");

        Log.d(AppConstants.TAG, "" + keyValueManager.getBoolean("Hello", false));

    }

}
