package globant.com.keyvaluepoc.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import globant.com.keyvaluepoc.AppConstants;

/**
 * Created by jayesh on 25/5/16.
 */
public class DBDao {

    private static Context mContext;
    private static DBDao mDbDao;
    private static SQLiteDatabase mSQLiteDatabase;

    /**
     * To get the singleton instance of database operations
     *
     * @return
     */
    public static DBDao getInstance(Context context) {
        mContext = context;
        if (mDbDao == null) {
            synchronized (DBDao.class) {
                if (mDbDao == null) {
                    mDbDao = new DBDao();
                }
            }
        }
        return mDbDao;
    }

    /**
     * To get the singleton instance of database helper class
     *
     * @return
     */
    public static SQLiteDatabase getSQLiteDatabase() {
        if (mSQLiteDatabase == null) {
            synchronized (SQLiteDatabase.class) {
                if (mSQLiteDatabase == null) {
                    mSQLiteDatabase = DBHelper.getInstance(mContext)
                            .getWritableDatabase();
                }
            }
        }
        return mSQLiteDatabase;
    }

    /**
     * To insert given data into database
     *
     * @param key
     * @param value
     * @return
     */
    synchronized public long insertInto(String key, String value) {
        long result = -1;
        try {
            getSQLiteDatabase().beginTransaction();

            ContentValues cv = new ContentValues();
            cv.put(DBConstants.COLUMN_KEY, key);
            cv.put(DBConstants.COLUMN_VAL, value);

            result = DBDao.getSQLiteDatabase().insert(
                    DBConstants.TABLE_KV, null, cv);
            DBDao.getSQLiteDatabase().setTransactionSuccessful();

        } catch (IllegalStateException e) {
            Log.e("TAG", e.getStackTrace().toString());
        } finally {
            DBDao.getSQLiteDatabase().endTransaction();
        }
        return result;
    }

    /**
     * To retrieve the data from database based on give key
     *
     * @param key
     * @return
     */
    synchronized public String readFrom(String key) {
        String output = null;
        try {

            String selectQuery = "SELECT * FROM " + DBConstants.TABLE_KV + " " +
                    "WHERE " + DBConstants.COLUMN_KEY + " = '" + key + "'";

            Cursor cursor = DBDao.getSQLiteDatabase().rawQuery(selectQuery,
                    null);

            if (cursor != null && cursor.getCount() != 0) {
                cursor.moveToFirst();
                output = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_VAL));
            } else {
                return null;
            }

            if (cursor != null) {
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }


        } catch (IllegalStateException e) {
            Log.e("TAG", e.getStackTrace().toString());
        }

        return output;
    }


    /**
     * To check whether given key is present in DB or not
     *
     * @param key
     * @return
     */
    synchronized public boolean isKeyPresent(String key) {
        boolean returnValue;
        Cursor cur = null;
        try {

            cur = getSQLiteDatabase().rawQuery(
                    "SELECT COUNT(*) FROM " + DBConstants.TABLE_KV + " where "
                            + DBConstants.COLUMN_KEY + "= '" + key+"'", null);

        } catch (Exception e) {
            if (e == null) {
                Log.e("TAG", "Exception in " + this.getClass().getName());
            } else {
                Log.e("TAG", e.getMessage());
            }
        }

        if (cur != null) {
            cur.moveToFirst();
            if (cur.getCount() == 0 || cur.getInt(0) == 0) { // Zero count means
                // no row.
                returnValue = false;
            } else {
                returnValue = true;
            }
        } else {
            returnValue = false;
        }

        if (cur != null) {
            if (!cur.isClosed()) {
                cur.close();
            }
        }
        return returnValue;
    }


    /**
     * To update existing key with new value
     *
     * @param key
     * @param value
     * @return
     */
    synchronized public String updateExistingKey(String key, String value) {
        String output = null;
        try {
            getSQLiteDatabase().beginTransaction();

            ContentValues cv = new ContentValues();
            cv.put(DBConstants.COLUMN_VAL, value);

            DBDao.getSQLiteDatabase().update(DBConstants.TABLE_KV, cv, DBConstants.COLUMN_KEY + "=?", new String[]{key});

            DBDao.getSQLiteDatabase().setTransactionSuccessful();

        } catch (IllegalStateException e) {
            Log.e("TAG", e.getStackTrace().toString());
        } finally {
            DBDao.getSQLiteDatabase().endTransaction();
        }

        return output;
    }


    /**
     * To delete row by give key
     * @param key
     */
    synchronized public void deleteRow(String key){

        mDbDao.getSQLiteDatabase().delete(DBConstants.TABLE_KV,
                DBConstants.COLUMN_KEY+ " = ?", new String[]{key});

    }


    /**
     * To empty table
     *
     * @param tableName
     * @return
     */
    synchronized public void truncateTable(String tableName) {
        try {
            getSQLiteDatabase().execSQL("delete from " + tableName);
        } catch (SQLException e) {
            if (e == null) {
                Log.e(AppConstants.TAG, "SQLException in " + this.getClass().getName());
            } else {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }
}