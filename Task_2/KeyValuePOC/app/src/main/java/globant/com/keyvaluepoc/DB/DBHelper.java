package globant.com.keyvaluepoc.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jayesh on 23/5/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper sDatabaseOpenHelper;
    private Context mContext;

    private DBHelper(Context context) {
        super(context, DBConstants.DATABASE_NAME, null, DBConstants.DATABASE_VERSION);
        mContext = context;
    }

    /**
     * Get the Singleton instance of DBHelper class
     *
     * @param context the application context
     * @return the Singleton DatabaseOpenHelper class
     */
    public static DBHelper getInstance(Context context) {
        if (sDatabaseOpenHelper == null) {
            synchronized (DBHelper.class) {
                if (sDatabaseOpenHelper == null) {
                    sDatabaseOpenHelper = new DBHelper(context);
                }
            }
        }
        return sDatabaseOpenHelper;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBConstants.SQL_CREATE_TABLE_KV);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DBConstants.SQL_DELETE_TABLE_KV);
    }
}
