package globant.com.keyvaluepoc.DB;

/**
 * Created by jayesh on 23/5/16.
 */
public interface DBConstants {

    String DATABASE_NAME = "keyval.db";
    int DATABASE_VERSION = 1;


    /**
     * KV Table
     */
    String TABLE_KV = "kv";

    // KVs COLUMN's
    String COLUMN_KEY = "key";
    String COLUMN_VAL = "value";


    // KV Table Creation Query
    String SQL_CREATE_TABLE_KV = "CREATE TABLE "
            + TABLE_KV + " (" +
            COLUMN_KEY + " TEXT PRIMARY KEY, " +
            COLUMN_VAL + " TEXT );";


    // Drop KV Table
    String SQL_DELETE_TABLE_KV =
            "DROP TABLE IF EXISTS " + TABLE_KV;

}
