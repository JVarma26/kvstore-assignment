package globant.com.keyvaluepoc;

import android.content.Context;

import globant.com.keyvaluepoc.DB.DBDao;

/**
 * Created by jayesh on 23/5/16.
 */

public class KeyValueManager {

    private static KeyValueManager keyValueManager;

    private static Context mContext;

    private DBDao mDbDao;

    private KeyValueManager() {
        mDbDao = DBDao.getInstance(mContext);
    }

    /**
     * To get the singleton instance
     *
     * @return
     */
    public static KeyValueManager getInstance(Context context) {
        mContext = context;
        if (keyValueManager == null) {
            synchronized (KeyValueManager.class) {
                if (keyValueManager == null) {
                    keyValueManager = new KeyValueManager();
                }
            }
        }
        return keyValueManager;
    }


    /**
     * @param key
     * @param val
     */
    public void putString(String key, String val) {
        if (!mDbDao.isKeyPresent(key)) {
            mDbDao.insertInto(key, val);
        } else {
            mDbDao.updateExistingKey(key, val);
        }

    }

    /**
     * @param key
     * @param val
     */
    public void putBoolean(String key, Boolean val) {
        if (!mDbDao.isKeyPresent(key)) {
            mDbDao.insertInto(key, String.valueOf(val));
        } else {
            mDbDao.updateExistingKey(key, String.valueOf(val));
        }
    }

    /**
     * @param key
     * @param val
     */
    public void putInt(String key, Integer val) {
        if (!mDbDao.isKeyPresent(key)) {
            mDbDao.insertInto(key, String.valueOf(val));
        } else {
            mDbDao.updateExistingKey(key, String.valueOf(val));
        }
    }

    /**
     * @param key
     * @param val
     */
    public void putLong(String key, Long val) {
        if (!mDbDao.isKeyPresent(key)) {
            mDbDao.insertInto(key, String.valueOf(val));
        } else {
            mDbDao.updateExistingKey(key, String.valueOf(val));
        }
    }

    /**
     * @param key
     * @param val
     */
    public void putDouble(String key, Double val) {
        if (!mDbDao.isKeyPresent(key)) {
            mDbDao.insertInto(key, String.valueOf(val));
        } else {
            mDbDao.updateExistingKey(key, String.valueOf(val));
        }
    }

    /* ############################################## */

    /**
     * @param key
     * @return
     */
    String getString(String key, String defVal) {
        String result = mDbDao.readFrom(key);
        return (result != null ? result : defVal);
    }

    /**
     * @param key
     * @return
     */
    boolean getBoolean(String key, boolean defVal) {
        String result = mDbDao.readFrom(key);
        return (result != null ? Boolean.parseBoolean(result) : defVal);
    }

    /**
     * @param key
     * @return
     */
    int getInt(String key, int defVal) throws Exception{
        String result = mDbDao.readFrom(key);
        return (result != null ? Integer.parseInt(result) : defVal);
    }

    /**
     * @param key
     * @return
     */
    long getLong(String key, long defVal) throws Exception{
        String result = mDbDao.readFrom(key);
        return (result != null ? Long.parseLong(result) : defVal);
    }

    /**
     * @param key
     * @return
     */
    Double getDouble(String key, Double defVal) throws Exception{
        String result = mDbDao.readFrom(key);
        return (result != null ? Double.parseDouble(result) : defVal);
    }
}
