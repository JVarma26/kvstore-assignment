package globant.com.keyvaluepoc;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import globant.com.keyvaluepoc.DB.DBConstants;
import globant.com.keyvaluepoc.DB.DBDao;

/**
 * Created by jayesh on 27/5/16.
 */
public class DBDAOTestCases extends InstrumentationTestCase {

    private Context mContext;
    private DBDao mDbDao;
    private KeyValueManager keyValueManager;

    private String stringKey = "stringKey";
    private String booleanKey = "booleanKey";
    private String intKey = "intKey";
    private String doubleKey = "doubleKey";
    private String longKey = "longKey";

    private String stringData = "testString";
    private boolean booleanData = true;
    private int intData = 10;
    private Double doubleData = 1.22;
    private long longData = 1234567890;


    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mContext = getInstrumentation().getTargetContext();
        mDbDao = DBDao.getInstance(mContext);
        keyValueManager = KeyValueManager.getInstance(mContext);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * To test whether data is inserted in database or not
     *
     * @throws Exception
     */
    public void testInsertion() throws Exception {

        // insert data
        keyValueManager.putString(stringKey, stringData);

        // verify if the inserted key is present in database
        assertTrue("database must contain inserted key", mDbDao.isKeyPresent(stringKey));

        // verify if the fetched value is same as inserted
        assertEquals("\"Key value fetched from database must be same as dummy insertion\" ",
                stringData, keyValueManager.getString(stringKey, "defVal"));
    }

    /**
     * To verify data in database without insertion
     *
     * @throws Exception
     */
    public void testNonInsertion() throws Exception {

        // insert data
        mDbDao.truncateTable(DBConstants.TABLE_KV);

        // verify if the inserted key is present in database
        assertFalse("database doesn't contain deleted key", mDbDao.isKeyPresent(booleanKey));

    }


    /**
     * To test whether data is deleted
     *
     * @throws Exception
     */
    public void testDelete() throws Exception {

        mDbDao.truncateTable(DBConstants.TABLE_KV);

        // add data to DB
        keyValueManager.putString(stringKey, stringData);

        // delete data from DB
        mDbDao.deleteRow(stringKey);

        // verify if the deleted data is present in DB
        assertFalse("database does not contain deleted data", mDbDao.isKeyPresent(stringKey));

    }

    /**
     * To test return type
     */
    public void testDataType() throws Exception {

        // add data to DB
        keyValueManager.putBoolean(booleanKey, booleanData);
        keyValueManager.putInt(intKey, intData);
        keyValueManager.putDouble(doubleKey, doubleData);
        keyValueManager.putLong(longKey, longData);

        assertEquals("database does not contain deleted data", booleanData, keyValueManager.getBoolean(booleanKey, false));
        assertEquals("database does not contain deleted data", intData, keyValueManager.getInt(intKey, 0));
        assertEquals("database does not contain deleted data", doubleData, keyValueManager.getDouble(doubleKey, 0.0));
        assertEquals("database does not contain deleted data", longData, keyValueManager.getLong(longKey, 0000));
    }


    /**
     * To test data is update or not
     */
    public void testKeyUpdate() {

        // truncate table
        mDbDao.truncateTable(DBConstants.TABLE_KV);

        keyValueManager.putBoolean(booleanKey, booleanData);
        keyValueManager.putInt(booleanKey, intData);

        // getBoolean  gives default value as updated value to booleanKey is not boolean
        assertNotSame("booleanKey value must update to intData", booleanData, keyValueManager.getBoolean(booleanKey, false));

        // data must be updated
        try {
            assertEquals("update data must return", intData, keyValueManager.getInt(booleanKey, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    /**
     * To test data is update or not
     */
    public void testKeyUpdateWithInt() {

        // truncate table
        mDbDao.truncateTable(DBConstants.TABLE_KV);

        keyValueManager.putInt(stringKey, intData);
        keyValueManager.putBoolean(stringKey, booleanData);

        // getBoolean  gives default value as updated value to booleanKey is not boolean
        try {
            assertNotSame("booleanKey value must update to intData", booleanData, keyValueManager.getInt(stringKey, 0));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // data must be updated
        assertEquals("update data must return", booleanData, keyValueManager.getBoolean(stringKey, false));

    }



    /**
     * To Test using thread executor
     */
    public void testUsingExecutor() {
        final ExecutorService executorService = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 10; i++) {
            Runnable worker = new WorkerThread("" + i);
            executorService.execute(worker);//calling execute method of ExecutorService
        }

        executorService.shutdown();
        while (!executorService.isTerminated()) {
        }

    }


    class WorkerThread implements Runnable {
        private String message;

        public WorkerThread(String s) {
            this.message = s;
        }

        public void run() {
            Log.d("TAG1", Thread.currentThread().getName() + " (Start) message = " + message);
            //call processmessage method that sleeps the thread for 2 seconds
            processmessage();
            Log.d("TAG1", Thread.currentThread().getName() + " (End)");
        }

        private void processmessage() {
            try {
                Thread.sleep(2000);

                keyValueManager.putString("2", "hello1");
                keyValueManager.putBoolean("abc", true);

                Log.d("TAG1", keyValueManager.getString("2", "defVal"));
                Log.d("TAG1", keyValueManager.getString("abc", "defVal"));

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
